const ownUserInfo = (req) => {
  console.log('TCL: ownInfo -> req', req)
  return [200, {
    status: 'OK',
    code: '000',
    message: 'Request success',
    data: {
      user: {
        name: 'balabala',
        role: 'admin'
      }
    }
  }]
}

export {
  ownUserInfo
}
