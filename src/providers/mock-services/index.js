import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import urls from '../services/urls'

import * as auth from './auth'
import * as user from './user'

const mock = new MockAdapter(axios)
// const mock = new MockAdapter(axios, { delayResponse: 2000 });

mock.onPost(urls.auth).reply(auth.login)
mock.onGet(urls.ownUserInfo).reply(user.ownUserInfo)
