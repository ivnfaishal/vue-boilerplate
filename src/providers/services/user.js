import axios from 'axios'
import urls from './urls'

const myInfo = async function () {
  try {
    const res = await axios.get(urls.ownUserInfo)
    console.log('TCL: res', res)
    return res.data
  } catch (error) {
    console.log('TCL: }catch -> error', error)
    return Promise.reject(error)
  }
}

export {
  myInfo
}
