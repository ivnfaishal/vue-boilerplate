import axios from 'axios'
import urls from './urls'

const login = async function ({ username, password }) {
  try {
    let data = {
      username,
      password
    }
    const res = await axios.post(urls.auth, data)
    console.log('TCL: login -> res', res)
    return res.data
  } catch (error) {
    console.log('TCL: login -> catch -> error', error)
    return Promise.reject(error)
  }
}

export {
  login
}
