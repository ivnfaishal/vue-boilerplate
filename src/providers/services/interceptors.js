import axios from 'axios'
import store from '../../store'

export default function setup () {
  axios.interceptors.request.use(function (config) {
    console.log('TCL: setup -> store.auth.state', store.state.auth)
    const token = store.state.auth.token
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  }, function (err) {
    return Promise.reject(err)
  })

  axios.interceptors.response.use((response) => { // intercept the global error
    return response
  }, function (error) {
    console.log('response ERROR', error.response)
    let originalRequest = error.config
    if (error.response && error.response.status === 401) {
      originalRequest._retry = true
      store.dispatch('auth/logout')
      alert('something is wrong please login again.')
    }
    // Do something with response error
    return Promise.reject(error)
  })
}
