export default {
  auth: '/auth',
  ownUserInfo: '/me'
}
