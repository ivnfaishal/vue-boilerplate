import axios from 'axios'
import interceptorSetup from './interceptors'

import * as auth from './auth'
import * as user from './user'

if (process.env.VUE_APP_API_TARGET === 'mock') {
  require('../mock-services') // import mock adapter if env is mock
}

axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL
interceptorSetup()

export {
  auth,
  user
}
