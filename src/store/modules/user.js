const state = {
  myUserInfo: null
}

const getters = {
}

const mutations = {
  set_my_user_info (store, data) {
    store.myUserInfo = data
  },
  reset_my_user_info (store) {
    store.myUserInfo = null
  }
}

const actions = {
  async fetchMyInfo ({ commit }) {
    try {
      const res = await this._vm.$_services.user.myInfo()
      commit('set_my_user_info', res.data.user)
      return res
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

export default {
  namespaced: true,
  getters,
  state,
  actions,
  mutations
}
