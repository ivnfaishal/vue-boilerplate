import router from '../../router'
const state = {
  token: localStorage.getItem('token') || ''
}

const getters = {
  isLoggedIn: state => !!state.token
}

const mutations = {
  set_token (store, data) {
    store.token = data
  },
  logout (store) {
    localStorage.removeItem('token')
    store.token = ''
  }
}

const actions = {
  async login ({ commit }, data) {
    try {
      console.log('TCL: login -> this', this._vm)
      const loginRes = await this._vm.$_services.auth.login(data)
      console.log('TCL: login -> user', loginRes)
      commit('set_token', loginRes.data.token)
      localStorage.setItem('token', loginRes.data.token)
      return loginRes
    } catch (error) {
      console.log('TCL: actions -> login -> catch -> error', error)
      return Promise.reject(error)
    }
  },
  logout ({ commit }) {
    commit('logout')
    commit('user/reset_my_user_info', null, { root: true })
    router.push('/login')
  }
}

export default {
  namespaced: true,
  getters,
  state,
  actions,
  mutations
}
