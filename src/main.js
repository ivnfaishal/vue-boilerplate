import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/styles/common.scss'

import * as services from './providers/services'

Vue.config.productionTip = false

console.log('process.env.VUE_APP_API_TARGET', process.env.VUE_APP_API_TARGET)

Vue.prototype.$_services = services

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
