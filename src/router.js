import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import About from './views/About.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta: {
        title: 'Home',
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: 'Login',
        rejectAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      meta: {
        title: 'About'
      }
    },
    {
      path: '*',
      redirect: '/login'
    }
  ]
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || ''
  console.log('TCL: to', to)
  if (to.meta.requiresAuth) {
    console.log('TCL: store.auth.getters', store.getters['auth/isLoggedIn'])
    if (store.getters['auth/isLoggedIn']) {
      next()
      return
    }
    next('/login')
  } else if (to.meta.rejectAuth) {
    if (store.getters['auth/isLoggedIn']) {
      next('/home')
      return
    }
    next()
  } else {
    next()
  }
})

export default router
