FROM node:latest

RUN curl -o- -L https://yarnpkg.com/install.sh | bash

RUN yarn global add @vue/cli

RUN mkdir /app
COPY . /app

WORKDIR /app