# vuetest

Vue simple boiler plate with added:
 - docker-compose
 - axios
 - axios mock
 - multiple api target
 - authentication with localStorage
 - router protected page

## Project setup
```
docker-compose run install
```

### Compiles and hot-reloads for development with different api target
```
docker-compose up mock

docker-compose up sandbox

docker-compose up prod
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
